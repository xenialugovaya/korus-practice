class Types {
  constructor(value){
    this.value = value;
  }

  isNum(value = this.value){
    return Number.isFinite(value);
  }

  isNan(value = this.value){
    return Number.isNaN(value);
  }

  isStr(value = this.value){
    return typeof value === 'string';
  }

  isBool(value = this.value){
    return typeof value === 'boolean';
  }

  isNull(value = this.value){
    return value === null;
  }

  isUndef(value = this.value){
    return value === undefined;
  }

  isObj(value = this.value){
    return value !== null && typeof value === 'object';
  }

  toNum(value = this.value){
    if(this.isNan(Number(value)) && this.isObj(value)){
      throw new Error('Failed to convert the object type to a number. ValueOf() method in object is not overridden');
    }
    return this.isNan(Number(value)) ? 0 : Number(value);
  }

  toStr(value = this.value){
    if(String(value) === '[object Object]'){
      throw new Error('Failed to convert the object type to a string. toString() method in object is not overridden');
    }
    return String(value);
  }

  toBool(value = this.value){
    return Boolean(value);
  }
}
    
export function types(value){
  return new Types(value);
}